import React, { Component } from "react";
import { View, Text, FlatList, Image, TouchableOpacity, AsyncStorage } from "react-native";
import Constants from "../Utils/Constants";
import styles from "../Utils/Styles";

class Products extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    return {
      headerTitle: "Products",
      headerRight: (
        <TouchableOpacity onPress={() => params.cartClicked()}>
          <Image style={styles.cartIcon} resizeMode="contain" source={require("../assets/cart.png")} />
          {params.count ? <Text style={styles.cartCount}>{params.count}</Text> : null}
        </TouchableOpacity>
      )
    };
  };

  async componentDidMount() {
    let count = await AsyncStorage.getItem("cartProducts");
    this.props.navigation.setParams({ cartClicked: this.onCartClick, count: JSON.parse(count).length });
  }

  onCartClick = () => {
    this.props.navigation.navigate("CartDetails");
  };

  render() {
    return (
      <View>
        <FlatList
          data={Constants.ENTRIES1}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => (
            <View style={{ flexDirection: "row", justifyContent: "space-between", margin: 15 }}>
              {2 * index + 1 <= Constants.ENTRIES1.length ? (
                <View style={{ backgroundColor: "white", margin: 5, borderRadius: 10 }}>
                  <Image
                    style={{ width: 120, height: 120 }}
                    resizeMode={"contain"}
                    source={{ uri: Constants.ENTRIES1[2 * index].image }}
                  />
                  <Text>{Constants.ENTRIES1[2 * index].name}</Text>
                </View>
              ) : null}
              {2 * index + 2 <= Constants.ENTRIES1.length ? (
                <View style={{ backgroundColor: "white", margin: 5, borderRadius: 10 }}>
                  <Image
                    style={{ width: 120, height: 120 }}
                    resizeMode={"contain"}
                    source={{ uri: Constants.ENTRIES1[2 * index + 1].image }}
                  />
                  <Text>{Constants.ENTRIES1[2 * index + 1].name}</Text>
                </View>
              ) : null}
            </View>
          )}
        />
      </View>
    );
  }
}

export default Products;
