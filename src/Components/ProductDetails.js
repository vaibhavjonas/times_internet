import React, { Component } from "react";
import { View, Text, Dimensions, Image, TouchableOpacity, AsyncStorage } from "react-native";
import Carousel, { Pagination } from "react-native-snap-carousel";
import Constants from "../Utils/Constants";
import styles from "../Utils/Styles";

const { width: viewportWidth, height: viewportHeight } = Dimensions.get("window");

class ProductDetails extends Component {
  state = {
    activeSlideIndex: 0,
    total: 0,
    products: []
  };

  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    return {
      headerTitle: "Product Details",
      headerRight: (
        <TouchableOpacity onPress={() => params.cartClicked()}>
          <Image style={styles.cartIcon} resizeMode="contain" source={require("../assets/cart.png")} />
          {params.count ? <Text style={styles.cartCount}>{params.count}</Text> : null}
        </TouchableOpacity>
      )
    };
  };

  async componentDidMount() {
    let products = await AsyncStorage.getItem("cartProducts");
    this.props.navigation.setParams({ cartClicked: this.onCartClick, count: JSON.parse(products).length });
    this.setState({ products: JSON.parse(products) });
  }

  onCartClick = () => {
    this.props.navigation.navigate("CartDetails");
  };

  renderItem({ item, index }) {
    return (
      <View style={{ height: viewportHeight * 0.3 }}>
        <Image style={{ height: "100%", width: "100%" }} resizeMode={"contain"} source={{ uri: Constants.BANNERS[index] }} />
      </View>
    );
  }

  buyNowClicked = async () => {
    let products = JSON.parse(await AsyncStorage.getItem("cartProducts"));
    let newProducts = !!products
      ? [...products, this.props.navigation.state.params.item]
      : [this.props.navigation.state.params.item];
    this.props.navigation.setParams({ count: newProducts.length });
    AsyncStorage.setItem("cartProducts", JSON.stringify(newProducts));
    this.props.navigation.navigate("CartDetails");
  };

  addToCart = async () => {
    let products = JSON.parse(await AsyncStorage.getItem("cartProducts"));
    let newProducts = !!products
      ? [...products, this.props.navigation.state.params.item]
      : [this.props.navigation.state.params.item];
    this.props.navigation.setParams({ count: newProducts.length });
    AsyncStorage.setItem("cartProducts", JSON.stringify(newProducts));
    alert("Item added successfully.");
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View>
          <Carousel
            data={Constants.BANNERS}
            renderItem={this.renderItem}
            sliderWidth={viewportWidth}
            itemWidth={viewportWidth}
            slideStyle={{ width: viewportWidth }}
            inactiveSlideOpacity={1}
            inactiveSlideScale={1}
            onSnapToItem={index => this.setState({ activeSlideIndex: index })}
          />
          <Pagination
            dotColor={"red"}
            inactiveDotColor={"grey"}
            dotsLength={Constants.BANNERS.length}
            containerStyle={{ paddingVertical: 2 }}
            activeDotIndex={this.state.activeSlideIndex}
          />
        </View>
        <View style={{ margin: 20 }}>
          <Text style={{ fontSize: 20, marginBottom: 20 }}>{this.props.navigation.state.params.item.name}</Text>
          <Text style={{ fontSize: 15 }}>{"INR " + this.props.navigation.state.params.item.cost}</Text>
        </View>
        <View style={{ position: "absolute", bottom: 0, flexDirection: "row" }}>
          <TouchableOpacity onPress={() => this.addToCart()} style={{ flex: 0.5, padding: 20, backgroundColor: "#F0FFDE" }}>
            <Text style={{ fontSize: 20, textAlign: "center" }}>{"Add To Cart"}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.buyNowClicked()}
            style={{ flex: 0.5, padding: 20, backgroundColor: "#DEFFFC" }}>
            <Text style={{ fontSize: 20, textAlign: "center" }}>{"Buy Now"}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default ProductDetails;
