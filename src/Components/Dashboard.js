import React, { Component } from "react";
import { View, Text, FlatList, Dimensions, Image, TouchableOpacity, AsyncStorage } from "react-native";
import Carousel, { Pagination } from "react-native-snap-carousel";
import Constants from "../Utils/Constants";
import styles from "../Utils/Styles";

const { width: viewportWidth, height: viewportHeight } = Dimensions.get("window");

class Dashboard extends Component {
  state = {
    activeSlideIndex: 0
  };

  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    return {
      headerTitle: "TIL Test",
      headerRight: (
        <TouchableOpacity onPress={() => params.cartClicked()}>
          <Image style={styles.cartIcon} resizeMode="contain" source={require("../assets/cart.png")} />
          {params.count ? <Text style={styles.cartCount}>{params.count}</Text> : null}
        </TouchableOpacity>
      )
    };
  };

  componentDidMount() {
    this.didFocusSubscription = this.props.navigation.addListener("didFocus", payload => {
      AsyncStorage.getItem("cartProducts").then(count => {
        this.props.navigation.setParams({ cartClicked: this.onCartClick, count: JSON.parse(count).length });
      });
    });
  }

  onCartClick = () => {
    this.props.navigation.navigate("CartDetails");
  };

  renderItem({ item, index }) {
    return (
      <View style={{ height: viewportHeight * 0.3 }}>
        <Image style={{ height: "100%", width: "100%" }} resizeMode={"contain"} source={{ uri: Constants.BANNERS[index] }} />
      </View>
    );
  }

  viewMoreClicked = () => {
    this.props.navigation.navigate("Products");
  };

  gotoProductDetails = item => {
    this.props.navigation.navigate("ProductDetails", { item: item });
  };

  componentWillUnmount() {
    this.didFocusSubscription.remove();
  }

  render() {
    return (
      <View>
        <View>
          <Carousel
            data={Constants.BANNERS}
            renderItem={this.renderItem}
            sliderWidth={viewportWidth}
            itemWidth={viewportWidth}
            slideStyle={{ width: viewportWidth }}
            inactiveSlideOpacity={1}
            inactiveSlideScale={1}
            onSnapToItem={index => this.setState({ activeSlideIndex: index })}
          />
          <Pagination
            dotColor={"red"}
            inactiveDotColor={"grey"}
            dotsLength={Constants.BANNERS.length}
            containerStyle={{ paddingVertical: 2 }}
            activeDotIndex={this.state.activeSlideIndex}
          />
        </View>
        <View style={{ margin: 7, marginTop: 20 }}>
          <View style={styles.headerStyle}>
            <Text style={{ fontSize: 15 }}>{"Featured Products"}</Text>
            <TouchableOpacity>
              <Text style={styles.viewMore} onPress={() => this.viewMoreClicked()}>
                {"View More"}
              </Text>
            </TouchableOpacity>
          </View>
          <FlatList
            data={Constants.ENTRIES1}
            horizontal={true}
            keyExtractor={(item, index) => index.toString()}
            showsHorizontalScrollIndicator={false}
            style={{ marginTop: 10, backgroundColor: "#D8D8D8" }}
            renderItem={({ item, index }) => (
              <TouchableOpacity
                onPress={() => this.gotoProductDetails(item)}
                style={{ backgroundColor: "white", margin: 5, borderRadius: 10 }}>
                <Image style={{ width: 90, height: 90 }} source={{ uri: item.image }} resizeMode={"contain"} />
                <Text style={{ textAlign: "center" }}>{item.name}</Text>
              </TouchableOpacity>
            )}
          />
        </View>
        <View style={{ margin: 7, marginTop: 20 }}>
          <View style={styles.headerStyle}>
            <Text style={{ fontSize: 15 }}>{"Latest Products"}</Text>
            <TouchableOpacity>
              <Text style={styles.viewMore} onPress={() => this.viewMoreClicked()}>
                {"View More"}
              </Text>
            </TouchableOpacity>
          </View>
          <FlatList
            data={Constants.ENTRIES1}
            showsHorizontalScrollIndicator={false}
            keyExtractor={(item, index) => index.toString()}
            horizontal={true}
            style={{ marginTop: 10, backgroundColor: "#D8D8D8" }}
            renderItem={({ item }) => (
              <TouchableOpacity
                onPress={() => this.gotoProductDetails(item)}
                style={{ backgroundColor: "white", margin: 5, borderRadius: 10 }}>
                <Image style={{ width: 90, height: 90 }} resizeMode={"contain"} source={{ uri: item.image }} />
                <Text style={{ textAlign: "center" }}>{item.name}</Text>
              </TouchableOpacity>
            )}
          />
        </View>
      </View>
    );
  }
}

export default Dashboard;
