import React, { Component } from "react";
import { View, Text, FlatList, Image, AsyncStorage, ScrollView } from "react-native";
import styles from "../Utils/Styles";

class CartDetails extends Component {
  state = {
    productData: [],
    filteredData: [],
    count: {},
    total: 0
  };
  static navigationOptions = {
    headerTitle: "Cart Details"
  };

  async componentDidMount() {
    // AsyncStorage.clear();
    let count = {};
    let productData = await AsyncStorage.getItem("cartProducts");
    if (!!productData) {
      productData = JSON.parse(productData);
      let total = productData.reduce((sum, obj) => obj.cost + sum, 0);
      productData.forEach(function(x) {
        count[x.name] = (count[x.name] || 0) + 1;
      });
      let uniq = {};
      var filteredData = productData.filter(obj => !uniq[obj.name] && (uniq[obj.name] = true));
      this.setState({ count, filteredData, total });
    }
  }

  render() {
    return (
      <ScrollView style={{ flex: 1 }}>
        <FlatList
          data={this.state.filteredData}
          keyExtractor={(item, index) => index.toString()}
          showsHorizontalScrollIndicator={false}
          style={{ marginTop: 10, backgroundColor: "#D8D8D8" }}
          renderItem={({ item, index }) => (
            <View style={{ backgroundColor: "white", margin: 5, borderRadius: 10, flexDirection: "row" }}>
              <Image style={{ width: 90, height: 90 }} source={{ uri: item.image }} resizeMode={"contain"} />
              <View style={{ marginLeft: 10 }}>
                <Text>{"Name: " + item.name}</Text>
                <Text>{"Quantity: " + this.state.count[item.name]}</Text>
                <Text>{"Cost: " + item.cost}</Text>
              </View>
            </View>
          )}
        />
        <View style={{ flexDirection: "row", margin: 20, justifyContent: "space-between" }}>
          <Text>{"Total:"}</Text>
          <Text>{this.state.total}</Text>
        </View>
      </ScrollView>
    );
  }
}

export default CartDetails;
