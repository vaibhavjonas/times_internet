module.exports = {
  ENTRIES1: [
    { image: "https://i.imgur.com/5mqmb5T.jpg", name: "test", cost: 1000 },
    { image: "https://i.imgur.com/2BkYLPI.jpg", name: "test1", cost: 2000 },
    { image: "https://i.imgur.com/AOVYvOJ.jpg", name: "test2", cost: 3000 },
    { image: "https://i.imgur.com/bZ8itAH.jpg", name: "test3", cost: 4000 },
    { image: "https://i.imgur.com/XgaZfXg.jpg", name: "test4", cost: 5000 },
    { image: "https://i.imgur.com/wZ21slD.jpg", name: "test5", cost: 6000 },
    { image: "https://i.imgur.com/h47oB5Y.jpg", name: "test6", cost: 7000 },
    { image: "https://i.imgur.com/ZZtWAh6.jpg", name: "test7", cost: 8000 },
    { image: "https://i.imgur.com/SFXcpQO.jpg", name: "test8", cost: 9000 },
    { image: "https://i.imgur.com/jr30y5M.jpg", name: "test9", cost: 10000 },
    { image: "https://i.imgur.com/aQyPOdC.jpg", name: "test10", cost: 11000 }
  ],
  BANNERS: [
    "https://i2.wp.com/batman-news.com/wp-content/uploads/2018/11/Aquaman-Official-Images-09.jpg",
    "http://www.collider.com/wp-content/uploads/inception_movie_poster_banner_01.jpg",
    "https://3.bp.blogspot.com/-SHlg1T_y0aQ/T2gOHZD7fCI/AAAAAAAAPK8/Ee5jU3DoUO8/s1600/The+Avengers+International+Movie+Banners+-+Hulk,+Hawkeye,+Maria+Hill,+Iron+Man,+Nick+Fury,+Captain+America,+Black+Widow+&+Thor.jpg"
  ]
};
