import { StyleSheet } from "react-native";

export default (styles = {
  cartIcon: {
    marginRight: 10,
    height: 30,
    width: 30
  },
  cartCount: {
    backgroundColor: "red",
    borderRadius: 10,
    textAlign: "center",
    position: "absolute",
    left: 20,
    bottom: 0,
    fontSize: 10,
    padding: 2
  },
  viewMore: {
    fontSize: 13,
    color: "blue"
  },
  headerStyle: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  }
});
