import { createStackNavigator, createAppContainer } from "react-navigation";
import Dashboard from "./Components/Dashboard";
import ProductDetails from "./Components/ProductDetails";
import CartDetails from "./Components/CartDetails";
import Products from "./Components/Products";

const stackNavigation = createStackNavigator(
  {
    Dashboard: { screen: Dashboard },
    ProductDetails: { screen: ProductDetails },
    Products: { screen: Products },
    CartDetails: { screen: CartDetails }
  },
  {
    navigationOptions: {
      headerTitleStyle: {
        textAlign: "center",
        flex: 1,
        color: "red"
      }
    }
  }
);

export default createAppContainer(stackNavigation);
